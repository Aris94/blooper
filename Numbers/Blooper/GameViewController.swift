//
//  GameViewController.swift
//  Numbers
//
//  Created by Aris Agdere on 4/29/16.
//  Copyright (c) 2016 aagdere1. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let menuScene = GameMenuScene(size: self.view.frame.size)
        let skView = self.view as! SKView
        menuScene.scaleMode = .aspectFill
        skView.showsFPS = false
        skView.showsNodeCount = false
        skView.presentScene(menuScene)
    }

    override var shouldAutorotate : Bool {
        return true
    }

    override var supportedInterfaceOrientations : UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    override var prefersStatusBarHidden : Bool {
        return true
    }
}
