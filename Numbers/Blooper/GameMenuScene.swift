//
//  GameMenuScene.swift
//  Blooper
//
//  Created by Aris Agdere on 5/21/16.
//  Copyright © 2016 aagdere1. All rights reserved.
//

import UIKit
import SpriteKit
import GameKit
import SCLAlertView

class GameMenuScene: GameEngine {
    
    var buttonWidth:CGFloat = 0.0
    var buttonHeight:CGFloat = 0.0
    var buttonSize:CGSize = CGSize()
    var clickButtonSound = SKAction.playSoundFileNamed("pop.mp3", waitForCompletion: false)
    var menuTitleLabel: SKLabelNode = SKLabelNode()
    var facebookInfoButton = SKShapeNode()
    var swipeCountLabel = SKLabelNode()
    var menuTitleColor = UIColor()
    let buttonBorderThickness:CGFloat = 2.0
    let buttonNames = ["Play","Tutorial","Leaderboard","button","facebookInfoButton"]
    let buttonFade:CGFloat = 0.7
    var buttonIsPressed = false
    var buttonPressedName = ""
    var buttonDistance:CGFloat = 0.115
    var buttonYLocation:CGFloat = 0.6
    var menuTitleLabelHeight:CGFloat = 0.8
    var bannerView = STABannerView()
    var fbPopupLaunched = false

    override func didMove(to view: SKView) {
        authenticateLocalPlayer()
        assignGlobalVariables()
        level = 50
        numBalls = 10
        setupView()
        
    }
    
    override func assignGlobalVariables(){
        super.assignGlobalVariables()
        screenWidth = UIScreen.main.bounds.width
        screenHeight = UIScreen.main.bounds.height
        self.size.width = screenWidth
        self.size.height = screenHeight
        buttonWidth = screenHeight * 0.35
        buttonHeight = screenWidth * 0.15
        menuTitleColor = ColorScheme.colorWithHexString("7cfc00")
        menuTitleColor = UIColor.black
    }
    
    override func setupView(){
        super.setupView()
        setupBackground()
        playSongAVPlayer("BlooperMenuHappy")
        setupBannerAd()
        makeButtons()
        setupLabels()
        setupFacebookHintButton()
        adjustWall()
        adjustBallVelocity()
        spawnWalls()
        spawnBalls()
    }
    
    func setupLabels(){
        setupLabel(menuTitleLabel, text: "Blooper", name: "titleLabel", bold: true, zPos: 106, fontName: "Arial Rounded MT ", fontSize: 60, fontColor: menuTitleColor, position: CGPoint(x: screenWidth * 0.5, y: screenHeight * menuTitleLabelHeight), withGradient: false)
        setupLabel(swipeCountLabel, text: "", name: "swipeLabel", bold: true, zPos: 106, fontName: "Arial Rounded MT ", fontSize: 30, fontColor: menuTitleColor, position: CGPoint(x: screenWidth * 0.5, y: screenHeight * (menuTitleLabelHeight - 0.09)), withGradient: false)
//        facebookInfoLabel.horizontalAlignmentMode = .Left
//        facebookInfoLabel.verticalAlignmentMode = .Center
//        setupLabel(facebookInfoLabel, text: "?", name: "facebookInfoLabel", bold: true, zPos: 106, fontName: "Arial Rounded MT ", fontSize: 30, fontColor: menuTitleColor, position: CGPoint(x: (screenWidth * 0.5) + (loginButton.frame.width / 2) + 10, y: (screenHeight * 0.2)), withGradient: false)
        menuTitleLabel.alpha = buttonFade
        swipeCountLabel.alpha = buttonFade
    }
    
    func setupFacebookHintButton(){
        facebookInfoButton = SKShapeNode(circleOfRadius: loginButton.frame.height / 3)
        facebookInfoButton.fillColor = ColorScheme.colorWithHexString("3b5998")
        facebookInfoButton.strokeColor = ColorScheme.colorWithHexString("3b5998")
        facebookInfoButton.name = "facebookInfoButton"
        facebookInfoButton.zPosition = 106
        facebookInfoButton.position = CGPoint(x: (screenWidth * 0.5) + (loginButton.frame.width / 2) + 15, y: (screenHeight * 0.2))
        
        let fbInvisibleCircle = SKShapeNode(circleOfRadius: loginButton.frame.height)
        fbInvisibleCircle.name = "facebookInfoButton"
        fbInvisibleCircle.zPosition = 106
        fbInvisibleCircle.position = CGPoint(x: (screenWidth * 0.5) + (loginButton.frame.width / 2) + 20, y: (screenHeight * 0.2))
        fbInvisibleCircle.fillColor = UIColor.clear
        fbInvisibleCircle.strokeColor = UIColor.clear
        addChild(fbInvisibleCircle)
        
        let facebookInfoButtonLabel = SKLabelNode(text: "?")
        facebookInfoButtonLabel.fontName = "Arial Rounded MT Bold"
        facebookInfoButtonLabel.fontSize = facebookInfoButton.frame.width - 10
        facebookInfoButtonLabel.position = CGPoint(x: 0, y: 0)
        facebookInfoButtonLabel.fontColor = UIColor.white
        facebookInfoButtonLabel.name = "ballLabel"
        facebookInfoButtonLabel.horizontalAlignmentMode = .center
        facebookInfoButtonLabel.verticalAlignmentMode = .center
        facebookInfoButton.addChild(facebookInfoButtonLabel)
        facebookInfoButtonLabel.zPosition = 1
        facebookInfoButton.alpha = buttonFade
        addChild(facebookInfoButton)
    }

    
    func setupBannerAd(){
        checkIfLoggedInFacebook()
        bannerView = STABannerView(size: STA_AutoAdSize, autoOrigin: STAAdOrigin_Bottom, with: self.view, withDelegate: nil)
        bannerView.alpha = 0.7
        self.view?.addSubview(bannerView)
    }
    
    func makeButtons(){
        makeButton(CGPoint(
            x: (screenWidth*0.5),
            y: (screenHeight*buttonYLocation)),
            text: "Play",
            color: UIColor.red,
            lineWidth: buttonBorderThickness,
            withPhysics: false)
        makeButton(CGPoint(
            x: (screenWidth*0.5),
            y: (screenHeight*(buttonYLocation-buttonDistance))),
            text: "Tutorial",
            color: UIColor.red,
            lineWidth: buttonBorderThickness,
            withPhysics: false)
        makeButton(CGPoint(
            x: (screenWidth*0.5),
            y: (screenHeight*(buttonYLocation-(2*buttonDistance)))),
            text: "Leaderboard",
            color: UIColor.red,
            lineWidth: buttonBorderThickness,
            withPhysics: false)
        setupFacebookButton(CGPoint(
            x: (screenWidth*0.5),
            y: (screenHeight*0.8)))
//        makeButton(CGPoint(
//            x: (screenWidth*0.5),
//            y: (screenHeight*0.8)),
//            text: "fb",
//            color: UIColor.clearColor(),
//            lineWidth: 5.0,
//            size: loginButton.frame.size,
//            zPos: 110,
//            flipLocation: true,
//            withPhysics: false)
    }
    
    func makeButton(_ position:CGPoint, text:String, color:UIColor, lineWidth:CGFloat = 2.0, size:CGSize = CGSize(), zPos:CGFloat = 107, flipLocation: Bool = false, withPhysics:Bool = true){
        let buttonSize:CGSize
        if(size == CGSize()){
            buttonSize = CGSize(width: buttonWidth, height: buttonHeight)
        }else{
            buttonSize = size
        }
        let button = SKShapeNode(rectOf: buttonSize)
        let label = SKLabelNode(text: text)
        
        label.fontName = "Arial Rounded MT Bold"
        label.fontSize = 25
        label.position = CGPoint(x: 0, y: 0)
        if(text == "fb"){
            label.fontColor = UIColor.clear
        }else{
            label.fontColor = UIColor.black
        }
        label.name = "buttonLabel"
        label.horizontalAlignmentMode = .center
        label.verticalAlignmentMode = .center
        label.zPosition = 1
        button.addChild(label)
        
        if(button.name == ""){
            button.name = "button"
        }else{
            button.name = text
        }
        button.path = CGPath(roundedRect: button.frame, cornerWidth: 10, cornerHeight: 10, transform: nil)
        if(flipLocation == false){
            button.position = position
        }else{
            button.position = CGPoint(x: position.x, y: (1-(position.y / screenHeight)) * screenHeight)
        }
        button.fillColor = color
        //button.strokeColor = UIColor.blackColor()
        button.strokeColor = ColorScheme.colorWithHexString("737373")
        button.lineWidth = lineWidth
        button.zPosition = zPos
        button.alpha = buttonFade
        
        if(withPhysics){
            button.physicsBody = SKPhysicsBody(polygonFrom: button.path!)
            button.physicsBody?.affectedByGravity = false
            button.physicsBody?.isDynamic = false
            button.physicsBody?.categoryBitMask = PhysicsCategory.wall
            button.physicsBody!.contactTestBitMask = PhysicsCategory.ball
            button.physicsBody!.collisionBitMask = PhysicsCategory.ball
            button.physicsBody!.friction = 0
            button.physicsBody?.restitution = 0
            button.physicsBody!.mass = 0
        }
        
        self.addChild(button)
    }
    
    func setupBackground(){
        self.backgroundColor = UIColor.white
    }
    
    func loadGame(){
        let gameScene = GameScene(size: self.view!.frame.size)
        let skView = self.view!
        gameScene.scaleMode = .aspectFill
        skView.showsFPS = false
        skView.showsNodeCount = false
        skView.presentScene(gameScene)
    }
    
    func loadTutorial(){
        let tutorialScene = TutorialScene(size: self.view!.frame.size)
        let skView = self.view!
        tutorialScene.scaleMode = .aspectFill
        skView.showsFPS = false
        skView.showsNodeCount = false
        skView.presentScene(tutorialScene)
    }
    
    func launchFacebookInfoPopup(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let popup = SCLAlertView(appearance: appearance)
        popup.addButton("Ok", target: self, selector: #selector(self.nothing))
        popup.showInfo("Login to Facebook", subTitle: "Login to Facebook to keep your level progress saved online!")
    }
    
    func displayLeaderboard(){
        self.showLeaderboard()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchLocation =  touches.first?.location(in: self)
        for node in nodes(at: touchLocation!){
            if(buttonNames.contains(node.name!)){
                if(node.name == "facebookInfoButton"){
                    facebookInfoButton.alpha = 1.0
                }
                node.alpha = 1.0
                buttonIsPressed = true
                buttonPressedName = node.name!
            }else if(node.name == "fb"){
                loginButton.alpha = 1.0
            }else if(["ball","powerUp", "powerDown"].contains(node.name!)){
                var ballSpriteNode = node as! SKSpriteNode
                removeBall(&ballSpriteNode, increment: false)
                incrementSwipeCountLabel()
                self.playSound(popSound)
                spawnRandomBall()
            }
        }
        removeTrail()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchLocation = touches.first?.location(in: self)
        if(buttonIsPressed == false){
            for node in nodes(at: touchLocation!){
                
                if((["ball","powerUp", "powerDown"].contains(node.name!)) && swipingEnabled){
                    var ballSpriteNode = node as! SKSpriteNode
                    self.playSound(popSound)
                    removeBall(&ballSpriteNode, increment: false)
                    incrementSwipeCountLabel()
                    spawnRandomBall()
                }
            }
            moveTouchToDrawWall(0, wallPointsIndex: 0, wallLengthIndex: 0, maxWallLengthIndex: 0, lineWidth:5, withPhysics: false, touchLocation: touchLocation!)
        }
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchLocation = touches.first?.location(in: self)
        for node in nodes(at: touchLocation!){
            
            if(buttonPressedName == node.name){
                switch(node.name!){
                    case "Play":
                        self.playSound(clickButtonSound, completion: {
                            self.loadGame()
                            self.bannerView.removeFromSuperview()
                        })
                        break
                    case "Leaderboard":
                        self.playSound(clickButtonSound, completion: {
                            self.displayLeaderboard()
                        })
                        break
                    case "Tutorial":
                        self.playSound(clickButtonSound, completion: {
                            self.loadTutorial()
                            self.bannerView.removeFromSuperview()
                        })
                        break
                    case "ball","powerUp","powerDown":
                        var ballSpriteNode = node as! SKSpriteNode
                        removeBall(&ballSpriteNode, increment: false)
                        incrementSwipeCountLabel()
                        self.playSound(popSound)
                        spawnRandomBall()
                        break
                    case "facebookInfoButton":
                        self.playSound(clickButtonSound, completion: {
                            if(self.fbPopupLaunched == false){
                                self.launchFacebookInfoPopup()
                                self.fbPopupLaunched = true
                                self.run(
                                    SKAction.sequence([
                                        SKAction.wait(forDuration: 0.01),
                                        SKAction.run({
                                            self.fbPopupLaunched = false
                                        })
                                    ])
                                )
    
                            }
                        })
                        break
                    default:
                        break
                }
            }
        }
        
        loginButton.alpha = buttonFade
        
        for node in self.children{
            if(buttonNames.contains(node.name!)){
                if(node.name == "facebookInfoButton"){
                    facebookInfoButton.alpha = buttonFade
                }
                node.alpha = buttonFade
            }
        }
        buttonIsPressed = false
        removeTrail()
    }
    
    override func willMove(from view: SKView) {
        loginButton.removeFromSuperview()
    }
    
    func incrementSwipeCountLabel(){
        if(swipeCountLabel.text == ""){
            swipeCountLabel.text = "1"
        }else{
            swipeCountLabel.text = "\(Int(swipeCountLabel.text!)! + 1)"
        }
    }
    
    func checkIfLoggedInFacebook(){
        if((FBSDKAccessToken.current()) != nil && STAStartAppSDK.sharedInstance().preferences.age == 0){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "age_range, gender"]).start(completionHandler: { (connection, result, error) -> Void in
                if (error == nil){
//                    let userName : NSString = result.valueForKey("name") as! NSString
                    let age_range : NSDictionary = ((result as AnyObject).value(forKey: "age_range") as! NSDictionary)
                    let age:UInt
                    let gender : NSString = (result as AnyObject).value(forKey: "gender") as! NSString
                    
                    let staGender:STAGender
                    if(gender == "female"){
                        staGender = STAGender_Female
                    }else{
                        staGender = STAGender_Male
                    }
                    
                    if(age_range.allValues.count >= 1){
                        age = age_range.allValues[0] as! UInt
                    }else{
                        age = 20
                    }
                    
                    STAStartAppSDK.sharedInstance().preferences = STASDKPreferences.prefrences(withAge: age, andGender: staGender)
                }
            })
        }
    }
    
}
