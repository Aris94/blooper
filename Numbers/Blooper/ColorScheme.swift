//
//  ColorScheme.swift
//  Blooper
//
//  Created by Aris Agdere on 5/31/16.
//  Copyright © 2016 aagdere1. All rights reserved.
//

import Foundation

class ColorScheme{
    
    static var red1:[UIColor] = ColorScheme.arrayWithColors([
        "ffcccc","ff9999","ff6666","ff3333","ff0000"
    ])
    static var orange2:[UIColor] = ColorScheme.arrayWithColors([
        "ffe6cc","ffcc99","ffb366","ff9933","ff8000"
    ])
    static var yellow3:[UIColor] = ColorScheme.arrayWithColors([
        "ffffcc","ffff99","ffff66","ffff33","ffff00"
    ])
//    static var lightGreen4:[UIColor] = ColorScheme.arrayWithColors([
//        "e6ffcc","ccff99","b3ff66","99ff33","80ff00"
//    ])
    static var limeGreen5:[UIColor] = ColorScheme.arrayWithColors([
        "ccffcc","99ff99","66ff66","33ff33","00ff00"
    ])
    static var turquoise6:[UIColor] = ColorScheme.arrayWithColors([
        "ccfff2","99ffe6","66ffd9","33ffcc","00ffbf",
    ])
    static var skyBlue7:[UIColor] = ColorScheme.arrayWithColors([
        "ccf2ff","99e6ff","66d9ff","33ccff","00bfff",
    ])
    static var navyBlue8:[UIColor] = ColorScheme.arrayWithColors([
        "ccccff","9999ff","6666ff","3333ff","0000ff"
    ])
    static var purple9:[UIColor] = ColorScheme.arrayWithColors([
        "e6ccff","cc99ff","b363ff","9933ff","8000ff",
    ])
    static var violet10:[UIColor] = ColorScheme.arrayWithColors([
        "ffccff","ff99ff","ff66ff","ff33ff","ff00ff"
    ])
    static var pink11:[UIColor] = ColorScheme.arrayWithColors([
        "ffcce6","ff99cc","ff66b3","ff3399","ff0080"
    ])

    static func arrayWithColors(_ hexVals:[String]) -> [UIColor]{
        var colors = [UIColor]()
        for hex in hexVals{
            colors.append(ColorScheme.colorWithHexString(hex))
        }
        return colors
    }
    
    static func getColorSchemes() -> [[UIColor]]{
        return [red1,orange2,yellow3,limeGreen5,turquoise6,skyBlue7,navyBlue8,purple9,violet10,pink11]
    }
    
    static func colorWithHexString (_ hex:String) -> UIColor {
        //var cString:String = hex.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet()).uppercased()
        var cString = hex
        
        if (cString.hasPrefix("#")) {
            cString = (cString as NSString).substring(from: 1)
        }
        
        if (cString.characters.count != 6) {
            return UIColor.gray
        }
        
        let rString = (cString as NSString).substring(to: 2)
        let gString = ((cString as NSString).substring(from: 2) as NSString).substring(to: 2)
        let bString = ((cString as NSString).substring(from: 4) as NSString).substring(to: 2)
        
        var r:CUnsignedInt = 0, g:CUnsignedInt = 0, b:CUnsignedInt = 0;
        Scanner(string: rString).scanHexInt32(&r)
        Scanner(string: gString).scanHexInt32(&g)
        Scanner(string: bString).scanHexInt32(&b)
        
        
        return UIColor(red: CGFloat(r) / 255.0, green: CGFloat(g) / 255.0, blue: CGFloat(b) / 255.0, alpha: CGFloat(1))
    }
    
    static func getComplementary(_ colour:UIColor) -> UIColor{
        let complementary:UIColor
        var hue:CGFloat = 0
        var saturation:CGFloat = 0
        var brightness:CGFloat = 0
        var alpha:CGFloat = 0
        colour.getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha)
        hue *= 360.0
        hue += 180.0
        hue = hue.truncatingRemainder(dividingBy: 360.0)
        hue /= 360.0
        //if(hue>360){hue-=360}
        if(alpha == 0.0){alpha = 1.0}
        complementary = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
        return complementary
    }
    
    static func equal(_ colour:UIColor, colour2:UIColor) -> Bool{
        var red:CGFloat = 0
        var green:CGFloat = 0
        var blue:CGFloat = 0
        var alpha:CGFloat = 0
        colour.getRed(&red, green: &green, blue: &blue, alpha: &alpha)
        
        var red2:CGFloat = 0
        var green2:CGFloat = 0
        var blue2:CGFloat = 0
        var alpha2:CGFloat = 0
        colour2.getRed(&red2, green: &green2, blue: &blue2, alpha: &alpha2)
        
        return (
            red == red2 &&
            green == green2 &&
            blue == blue2 &&
            alpha == alpha2
        )
    }
    
}
