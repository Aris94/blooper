//
//  GameScene.swift
//  Numbers
//
//  Created by Aris Agdere on 4/29/16.
//  Copyright (c) 2016 aagdere1. All rights reserved.
//

import SpriteKit
import GameKit
import AVFoundation
//import SCLAlertView

class GameScene: GameEngine {
    
    override func assignGlobalVariables(){
        testingModeEnabled = false
        super.assignGlobalVariables()
        updateDatabase()
        //currentGoalNumber = level
    }
    
    func setupTesting() {
        if(testingModeEnabled){
            previousHighScore = 1
            level = 100
            levelJump = 1
            powerUpPercentage = 1
            powerDownPercentage = 1
            loadPowerUps(false)
            shrinkPerClick = 10
            disableAllSounds = false
        }else{
            resetLevel = false
            if(resetLevel){
                defaults.removeObject(forKey: "PreviousHighScore")
            }
            previousHighScore = defaults.integer(forKey: "PreviousHighScore")
            level = previousHighScore + 1
            loadPowerUps(false)
        }
        pauseMenuLevel = level
        currentGoalNumber = Int(numberRange[0])
    }
    
    override func didMove(to view: SKView) {
        /* Setup your scene here */
        authenticateLocalPlayer()
        assignGlobalVariables()
        setupView()
        playSongAVPlayer("BlooperMenuHappy")
    }
    
    override func setupView(){
        super.setupView()
        setupTesting()
        setupLabels()
        setupPauseButton()
        spawnGoalBall(false)
        levelAdjustments()
        spawnBalls()
        spawnPullForce()
    }
    
    func setupLabels(){
        let levelLabelText = "\(level)"
        setupLabel(levelLabel, text: levelLabelText, name: "levelLabel", fontName: "Arial Rounded MT Bold", fontSize: 50, fontColor: customBlue, position: CGPoint(x: screenWidth * 0.5, y: screenHeight * 0.92), withGradient: true)
        setupLabel(powerUpLabel, text: "", name: "powerUpLabel",fontName: "Arial Rounded MT Bold", fontSize: 30, fontColor: neonGreen, position: CGPoint(x: screenWidth * 0.5, y: screenHeight * 0.01), withGradient: true)
        setupLabel(powerDownLabel, text: "", name: "powerDownLabel", fontName: "Arial Rounded MT Bold", fontSize: 30, fontColor: UIColor.red, position: CGPoint(x: screenWidth * 0.5, y: screenHeight * 0.01), withGradient: true)
//        setupLabel(pauseButton, text: "ll", name: "pauseButton", fontName: "Arial Rounded MT Bold", fontSize: 30, fontColor: UIColor.blackColor(), position: CGPoint(x: screenWidth * 0.95, y: screenHeight * 0.008), withGradient: false)
        
    }

}
