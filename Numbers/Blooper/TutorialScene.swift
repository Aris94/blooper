//
//  GameScene.swift
//  Numbers
//
//  Created by Aris Agdere on 4/29/16.
//  Copyright (c) 2016 aagdere1. All rights reserved.
//

import SpriteKit
import GameKit
import AVFoundation
import SCLAlertView

class TutorialScene: GameEngine {
    
    var tutorialNum = 0
    var labelFontSize:CGFloat = 25.0
    var tutorialLabelPositionX:CGFloat = 0.0
    var levelLabelPositionX:CGFloat = 0.0
    var tutorialAchievementPercentage = 0.0
    var tutorialName = ["Tapping", "Swiping", "Power Ups", "Power Downs"]
    var tutorialDescription = ["Press START and tap the center ball until its value reaches 0.",
                               "You may find it useful to swipe or tap the orange balls before they hit the center.",
                               "The green balls are power ups that grant you a random ability. You will unlock a new one every 10 levels until a certain level.",
                               "The red balls are power downs that stop you from being able to click the center ball. Avoid them by minimizing aimless swiping."
                               ]
    var tutorialLabel = SKLabelNode()
    var lastBackgroundColor = UIColor()
    
    override func assignGlobalVariables(){
        super.assignGlobalVariables()
        tutorialLabelPositionX = 0.49
        levelLabelPositionX = tutorialLabelPositionX + 0.02
    }
    
    override func didMove(to view: SKView) {
        /* Setup your scene here */
        authenticateLocalPlayer()
        assignGlobalVariables()
        setupView()
        playSongAVPlayer("BlooperMenuHappy")
    }
    
    override func setupView(){
        super.setupView()
        setupLabels()
        setupPauseButton()
        loadTutorial()
    }
    
    func setupLabels(){
        tutorialLabel.horizontalAlignmentMode = .right
        levelLabel.horizontalAlignmentMode = .left
        setupLabel(tutorialLabel, text: "Tutorial: ", name: "tutorialLabel", fontName: "Arial Rounded MT Bold", fontSize: labelFontSize, fontColor: customBlue, position: CGPoint(x: screenWidth * tutorialLabelPositionX, y: screenHeight * 0.95), withGradient: true)
        
        setupLabel(levelLabel, text: "\(tutorialName[tutorialNum])", name: "levelLabel", fontName: "Arial Rounded MT Bold", fontSize: labelFontSize, fontColor: neonGreen, position: CGPoint(x: (screenWidth * levelLabelPositionX), y: screenHeight * 0.95), withGradient: true)
        
        setupLabel(powerUpLabel, text: "", name: "powerUpLabel",fontName: "Arial Rounded MT Bold", fontSize: labelFontSize, fontColor: neonGreen, position: CGPoint(x: screenWidth * 0.5, y: screenHeight * 0.01), withGradient: true)
        setupLabel(powerDownLabel, text: "", name: "powerDownLabel", fontName: "Arial Rounded MT Bold", fontSize: labelFontSize, fontColor: UIColor.red, position: CGPoint(x: screenWidth * 0.5, y: screenHeight * 0.01), withGradient: true)
//        setupLabel(pauseButton, text: "ll", name: "pauseButton", fontName: "Arial Black", fontSize: 30, fontColor: UIColor.clearColor(), position: CGPoint(x: screenWidth * 0.95, y: screenHeight * 0.008), withGradient: true)
        
    }
    
    func moveTitleLabelsFromOriginal(_ shift:CGFloat){
        moveLabelWithGradient(tutorialLabel, x: (screenWidth * tutorialLabelPositionX) + shift, y: tutorialLabel.position.y)
        moveLabelWithGradient(levelLabel, x: (screenWidth * levelLabelPositionX) + shift, y: levelLabel.position.y)
    }
    
    func setupTappingTutorial(){
        level = 1
        numBalls = 5
        spawnFrequency = 2.0
        numberRange = [10,10]
        backgroundColor = colorScheme[2][1]
        moveTitleLabelsFromOriginal(-5)
    }
    
    func setupSwipingTutorial(){
        level = 5
        numBalls = 15
        maximumVelocity = 50
        spawnFrequency = 1.0
        numberRange = [20,20]
        backgroundColor = colorScheme[2][2]
    }
    
    func setupPowerUpTutorial(){
        level = 10
        numberRange = [30,30]
        maximumVelocity = 100
        unlockedPowerup = true
        powerUpPercentage = 30
        powerDownPercentage = 0
        powerUps = [PowerUp(nameIn: "Clear", descriptionIn: "All orange balls on the screen are popped.", timeIn: 0, soundActionIn: clearSound),
                    PowerUp(nameIn: "Freeze", descriptionIn: "All orange balls are frozen and new ones stop spawning.", timeIn: 3, soundActionIn:freezerSound)]
        powerUpAlert(powerUps.first!)
        backgroundColor = colorScheme[2][3]
        
        moveTitleLabelsFromOriginal(-18)

    }
    
    func setupPowerDownTutorial(){
        level = 10
        numberRange = [30,30]
        maximumVelocity = 100
        unlockedPowerup = false
        powerUpPercentage = 0
        powerDownPercentage = 50
        backgroundColor = colorScheme[2][4]

        moveTitleLabelsFromOriginal(-35)
    }
    
    func resetTutorial(){
        tutorialNum = 0
        loadTutorial()
    }
    
    func loadGame(){
        audioPlayer.stop()
        let gameScene = GameScene(size: self.view!.frame.size)
        let skView = self.view!
        gameScene.scaleMode = .aspectFill
        skView.showsFPS = false
        skView.showsNodeCount = false
        skView.presentScene(gameScene)
    }

    func finishTutorial(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alert:SCLAlertView = SCLAlertView(appearance: appearance)
        alert.addButton("Restart Tutorial", target: self, selector: #selector(TutorialScene.resetTutorial))
        alert.addButton("Play Blooper", target: self, selector: #selector(TutorialScene.loadGame))
        alert.addButton("Main Menu", target: self, selector: #selector(TutorialScene.mainMenu))
        alert.showSuccess("Tutorial Completed", subTitle: "Are you ready?")
    }
    
    func loadTutorial(){
        
        switch(tutorialNum){
            case 0:
                setupTappingTutorial()
                //levelLabel.position = CGPoint(x: screenWidth * 0.62, y: screenHeight * 0.95)
                break
            case 1:
                setupSwipingTutorial()
            case 2:
                setupPowerUpTutorial()
                //levelLabel.position = CGPoint(x: screenWidth * 0.66, y: screenHeight * 0.95)
            
            case 3:
                setupPowerDownTutorial()
            
            default:
                print("ERROR REACHED DEFAULT")
                break
        }
        
        adjustWall()
        adjustBallVelocity()
        restart(UIAlertAction())
        levelLabel.text = "\(tutorialName[tutorialNum])"
        enumerateChildNodes(withName: "levelLabelShadow", using: { node,stop in
            (node as! SKLabelNode).text = "\(self.tutorialName[self.tutorialNum])"
        })
        
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alert:SCLAlertView = SCLAlertView(appearance: appearance)
        alert.addButton("Try It", target: self, selector: #selector(TutorialScene.nothing))
        alert.showInfo("\(tutorialName[tutorialNum])", subTitle: tutorialDescription[tutorialNum])
        lastBackgroundColor = backgroundColor
        
    }
    
    override func gameWon() {
        tutorialAchievementPercentage += (100.0 * (1.0/Double(tutorialName.count)))
        if(100.0 - tutorialAchievementPercentage <= 1){
            tutorialAchievementPercentage = 100.0
        }
        print("Tutorial Percentage Complete: \(tutorialAchievementPercentage)")
        earnAchievement("Tutorial", percentage: tutorialAchievementPercentage, showBannerIfPercentUnder100: true)
        self.playSound(winSound)
        tutorialNum += 1
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alert:SCLAlertView = SCLAlertView(appearance: appearance)
        if(tutorialName.count > tutorialNum){
            alert.addButton("Next Lesson", target: self, selector: #selector(TutorialScene.loadTutorial))
        }else{
            alert.addButton("Ok", target: self, selector: #selector(TutorialScene.finishTutorial))
        }
        alert.showSuccess("\(tutorialName[tutorialNum-1])", subTitle: "The Blob Was Destroyed!")
    }
    
    override func pressPause(){
        pauseScene()
        pauseAlert = SCLAlertView(appearance: appearance)
        pauseAlert.addButton("Main Menu", target: self, selector: #selector(mainMenu))
        if(!musicIsPaused){
            pauseAlert.addButton("Music Off 🔇", target: self, selector: #selector(pauseMusic))
        }else{
            pauseAlert.addButton("Music On 🔊", target: self, selector: #selector(resumeMusic))
        }
        pauseAlert.addButton("Restart", target: self, selector: #selector(TutorialScene.restartLevel))
        pauseAlert.addButton("Resume", target: self, selector: #selector(resumeScene))
        pauseAlert.showInfo("Pause Menu", subTitle: "Tutorial: \(tutorialName[tutorialNum])", circleIconImage: pauseIcon)
    }
    
    override func restartLevel(){
        gameStarted = false
        resumeScene()
        loadTutorial()
    }
    
    override func adjustBackgroundColor() {
        self.backgroundColor = lastBackgroundColor
    }
    
}
