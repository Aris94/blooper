//
//  GameScene.swift
//  Numbers
//
//  Created by Aris Agdere on 4/29/16.
//  Copyright (c) 2016 aagdere1. All rights reserved.
//

import SpriteKit
import GameKit
import AVFoundation
import SCLAlertView
import CloudKit

struct PhysicsCategory {
    static let ball:UInt32              = 0b001
    static let wall:UInt32              = 0b010
    static let goalNumberBall:UInt32    = 0b100
}

class GameEngine: SKScene, SKPhysicsContactDelegate, GKGameCenterControllerDelegate  {
    
    var screenWidth:CGFloat = 0.0
    var screenHeight:CGFloat = 0.0
    var currentGoalNumber:Int = 0
    var audioPlayer = AVAudioPlayer()
    var levelLabel:SKLabelNode = SKLabelNode()
    var powerUpLabel:SKLabelNode = SKLabelNode()
    var powerDownLabel = SKLabelNode()
    var pauseLabel:SKLabelNode = SKLabelNode()
    var pauseButton:SKShapeNode = SKShapeNode()
    var disableAllSounds = false
    var currentPowerUp = PowerUp()
    var powerDown = false
    let goalPullForce = SKFieldNode.radialGravityField()
    let pullStrength:Float = 0.1
    var goalNumberBall = SKSpriteNode()
    var screenAdjustment:CGFloat = 0.0
    var numberRange:[UInt32] = [60,60]
    var ballVelocityRange:[CGFloat] = [0,0]
    var minimumSpawnFrequency:CGFloat = 0.10
    var minimumVelocity:CGFloat = 50
    var maximumVelocity:CGFloat = 100
    var initialBallVelocityRange:[CGFloat] = [100,-100]
    var currentScreenshot:UIImage? = nil
    var level = 1
    var levelJump = 1
    var levelsLost = 0
    var levelsWon = 0
    var hasClickedTimer = Timer()
    var indicatorTimer = Timer()
    var numBalls = 10
    var defaultGoalBallText = ""
    var pauseMenuLevel = 1
    var powerUps = [PowerUp]()
    var colorScheme = [[UIColor]]()
    var schemeIndex = 0
    var shadeIndex = 0
    var affectedBalls = [SKNode?]()
    var title = ""
    var neonGreen:UIColor = UIColor()
    var customBlue = UIColor()
    var balls = [SKSpriteNode]()
    var ballsImmune = false
    var gameStarted = false
    var ballsFrozen = false
    var ballsSlowed = false
    var swipingEnabled = true
    var powerUpsEnabled = true
    var doubleTap = false
    var musicStartsPaused = false
    var musicIsPaused = false
    var unlockedPowerup = false
    var powerUpPercentage = 2
    var powerDownPercentage = 2
    var powerUpIncrement = 10
    var powerUpMaxLevel = 60
    var minimumRadius:CGFloat = 0.0
    var ballNumReduction = 1.0
    var powerUpTimeLeft = 0
    var powerDownTimeLeft = 0
    var ballSpawnTimer = Timer()
    //var mainFont = "Arial Rounded MT Bold"
    var mainFont = "GeezaPro-Bold"
    var powerUpTimer = Timer()
    var powerDownTimer = Timer()
    var wallDistFromEdge:CGFloat = 0.0
    var initialSpawnFrequency:CGFloat = 1.0
    var spawnFrequency:CGFloat = 0.0
    var spawnFrequencyDecrease:CGFloat = 0.0
    var changedSpawnFrequencyDecrease:CGFloat = 0.002
    var initialSpawnFrequencyDecrease:CGFloat = 0.01
    var xOffset:CGFloat = 1.0
    var yOffset:CGFloat = 1.0
    var spawnFrequencyChangeover = 20
    var minimumLevel = 1
    var shrinkPerClick = 1
    var originalShrinkPerClick = 1
    let leaderboardID = "HighScore"
    var lastVelVectors = [CGVector]()
    var resetLevel = false
    let changeImageToRedBall = SKAction.setTexture(SKTexture(imageNamed: "redball.png"))
    let changeImageToLightBlueBall = SKAction.setTexture(SKTexture(imageNamed: "lightblueball.png"))
    let changeImageToDarkBlueBall = SKAction.setTexture(SKTexture(imageNamed: "darkblueball.png"))
    var wrongInput = false
    var winLevelPopup = SCLAlertView()
    
    var popSound = SKAction.playSoundFileNamed("pop.mp3", waitForCompletion: false)
    var powerDownSound = SKAction.playSoundFileNamed("fart.mp3", waitForCompletion: false)
    var tickSound = SKAction.playSoundFileNamed("tick.wav", waitForCompletion: false)
    var powerDownTickSound = SKAction.playSoundFileNamed("powerDownTickQuiet.mp3", waitForCompletion: false)
    var biteSound = SKAction.playSoundFileNamed("bite.wav", waitForCompletion: false)
    var winSound = SKAction.playSoundFileNamed("youWin.mp3", waitForCompletion: false)
    var loseSound = SKAction.playSoundFileNamed("youLose.mp3", waitForCompletion: false)
    var clearSound = SKAction.playSoundFileNamed("clear.wav", waitForCompletion: false)
    var wallerSound = SKAction.playSoundFileNamed("wall.wav", waitForCompletion: false)
    var freezerSound = SKAction.playSoundFileNamed("freeze.wav", waitForCompletion: false)
    var armorSound = SKAction.playSoundFileNamed("armor.wav", waitForCompletion: false)
    var doubleTapSound = SKAction.playSoundFileNamed("DoubleTap.mp3", waitForCompletion: false)
    var slowSound = SKAction.playSoundFileNamed("slomo2.mp3", waitForCompletion: false)
    
    var previousHighScore = 0
    var defaults = UserDefaults.standard
    var drawWallMode = false
    var pauseAlert = SCLAlertView()
    var levelSelectText = UITextField()
    var levelSelectAlert = SCLAlertView()
    var appearance = SCLAlertView.SCLAppearance()
    let pauseIcon = UIImage(named: "pause-icon.png")
    
    //    var trail = SKShapeNode()
    //    var drawnWall = SKShapeNode()
    var walls = [SKShapeNode(),SKShapeNode()]
    //    var trailPoints = [CGPoint]()
    //    var drawnWallPoints = [CGPoint]()
    var wallPoints = [[CGPoint](),[CGPoint]()]
    //    var trailLength:CGFloat = 0.0
    //    var drawnWallLength:CGFloat = 0.0
    var wallLengths:[CGFloat] = [0.0,0.0]
    //    var maxTrailLength:CGFloat = 0.0
    //    var maxDrawnWallLength:CGFloat = 0.0
    var maxWallLengths:[CGFloat] = [0.0,0.0]
    
    let loginButton: FBSDKLoginButton = {
        let button = FBSDKLoginButton()
        button.readPermissions = ["public_profile"]
        return button
    }()
    
    var testingModeEnabled = false
    
    func assignGlobalVariables(){
        screenWidth = UIScreen.main.bounds.width
        screenHeight = UIScreen.main.bounds.height
        screenAdjustment = (screenWidth / 375)
        self.size.width = screenWidth
        self.size.height = screenHeight
        minimumRadius = 30 * screenAdjustment
        wallDistFromEdge = minimumRadius * 4
        maxWallLengths = [screenWidth * 0.5, screenWidth * 1.5]
        spawnFrequencyDecrease = 0
        colorScheme = ColorScheme.getColorSchemes()
        neonGreen = ColorScheme.colorWithHexString("7cfc00")
        customBlue = ColorScheme.colorWithHexString("3399ff")
        defaultGoalBallText = "START"
        
        
        appearance = SCLAlertView.SCLAppearance(showCloseButton: false, showCircularIcon: true)
    
        
        if(defaults.bool(forKey: "musicOn")){
            musicIsPaused = false
        }else{
            musicIsPaused = true
            musicStartsPaused = true
        }
        pauseMenuLevel = level
        
    }
    
    override func didMove(to view: SKView) {
        /* Setup your scene here */
        authenticateLocalPlayer()
        assignGlobalVariables()
        setupView()
    }
    
    func setupView(){
        self.physicsWorld.gravity = CGVector(dx: 1e-100, dy: 1e-100)
        self.physicsWorld.contactDelegate = self
    }
    
    func setupLabel(_ label:SKLabelNode, text:String, name:String, bold:Bool = false, zPos:CGFloat = 102, fontName:String = "GeezaPro-Bold", fontSize:CGFloat, fontColor:UIColor, position:CGPoint, withGradient:Bool = false){
        label.text = text
        label.name = name
        label.zPosition = zPos
        if(bold){
            label.fontName = "\(fontName)Bold"
        }else{
            label.fontName = fontName
        }
        label.fontSize = fontSize
        label.fontColor = fontColor
        label.position = position
        addChild(label)
        if(withGradient){
            addLabelGradient(label)
        }
    }
    
    func setupPauseButton(){
//        setupLabel(pauseLabel, text: "ll", name: "pauseLabel", fontName: "Arial Rounded MT Bold", fontSize: 30, fontColor: UIColor.blackColor(), position: CGPoint(x: screenWidth * 0.95, y: screenHeight * 0.008), withGradient: false)
        
        pauseButton = SKShapeNode(circleOfRadius: screenWidth * 0.05)
        pauseButton.strokeColor = UIColor.black
        pauseButton.lineWidth = 4.0
        pauseButton.zPosition = 103
        pauseButton.position = CGPoint(x: screenWidth * 0.93, y: screenHeight * 0.04)
        pauseButton.name = "pauseButton"
        self.addChild(pauseButton)
        
        pauseLabel = SKLabelNode(text: "ll")
        pauseLabel.horizontalAlignmentMode = .center
        pauseLabel.verticalAlignmentMode = .center
        pauseLabel.fontName = "Arial Rounded MT Bold"
        pauseLabel.fontSize = 20
        pauseLabel.name = "pauseLabel"
        pauseLabel.zPosition = 1
        pauseLabel.position = CGPoint(x: 0, y: 0)
        pauseLabel.fontColor = UIColor.black
        
        pauseButton.addChild(pauseLabel)
        
        
    }
    
//    func setupPauseButton(){
//        let tex = SKTexture(imageNamed: "pause-icon.png")
//        let buttonDimension:CGFloat
//        if(screenWidth < screenHeight){
//            buttonDimension = screenWidth * 0.05
//        }else{
//            buttonDimension = screenHeight * 0.05
//        }
//        pauseButton = SKSpriteNode(texture: tex, size: CGSize(width: buttonDimension, height: buttonDimension))
//        pauseButton.position = CGPoint(x: screenWidth * 0.93, y: screenHeight * 0.008)
//        pauseButton.name = "pauseButton"
//        pauseButton.alpha = 0.7
//        self.addChild(pauseButton)
//    }
    
    func addLabelGradient(_ label:SKLabelNode){
        let newLabel = SKLabelNode()
        let gradientColor = UIColor.black
        newLabel.horizontalAlignmentMode = label.horizontalAlignmentMode
        newLabel.verticalAlignmentMode = label.verticalAlignmentMode
        setupLabel(newLabel, text: label.text!, name: "\(label.name!)Shadow", bold: false, fontName: label.fontName!, fontSize: label.fontSize, fontColor: gradientColor, position: CGPoint(x: label.position.x + xOffset, y: label.position.y - yOffset), withGradient: false)
        newLabel.zPosition = label.zPosition - 1
    }
    
    func setupFacebookButton(_ center:CGPoint) -> FBSDKLoginButton{
        self.view?.addSubview(loginButton)
        loginButton.center = center
        loginButton.alpha = 0.7
        return loginButton
    }
    
    func removeFacebookButton(){
        loginButton.removeFromSuperview()
    }
    
    func displayShareSheet(_ shareContent:[AnyObject]) {
        let activityViewController = ActivityViewController(activityItems: shareContent, applicationActivities: nil)
        
        self.presentViewController(activityViewController)
        
        //UIActivityViewControllerCompletionWithItemsHandler
        
        activityViewController.completionWithItemsHandler = {(activityType:UIActivityType?, completed:Bool, returnedItems:[Any]?, error: Error?) in
            self.launchWinAlertView()
        }
    }
    
    func updateDatabase(){
        if((FBSDKAccessToken.current()) != nil){
            FBSDKGraphRequest(graphPath: "me", parameters: ["fields": "id"]).start(completionHandler: { (connection, result, error) -> Void in
                let id : String = (result as AnyObject).value(forKey: "id") as! String
                self.saveHighscoreToDatabase(id)
            })
        }
    }
    
    func saveHighscoreToDatabase(_ id:String) {
        
        if((FBSDKAccessToken.current()) == nil) {
//            print("Not Logged In FB")
            return
        }
        
        let container = CKContainer.default()
        let publicDatabase = container.publicCloudDatabase
        
        publicDatabase.fetch(
            withRecordID: CKRecordID(recordName: id),
          completionHandler: ({record, error in
            if let err = error {
                print("Record could not be fetched: \(err)")
                let newRecord = CKRecord(recordType: "FacebookUser", recordID: CKRecordID(recordName: id))
                newRecord.setObject(self.previousHighScore as CKRecordValue?, forKey: "highscore")
                publicDatabase.save(newRecord, completionHandler:
                    ({returnRecord, error in
                        if let err = error {
                            print("Save Error: \(err.localizedDescription)")
                        } else {
//                            print("Success: Record \(returnRecord!.recordID.recordName) saved successfully")
                        }
                    })
                )
            } else {
                let savedHighscore = record!.object(forKey: "highscore") as! Int
//                print("Successfully fetched Record \(record!.recordID.recordName)")
                if(savedHighscore < self.previousHighScore){
                    //Save new highscore to database
                    record!.setObject(self.previousHighScore as CKRecordValue?, forKey: "highscore")
                    publicDatabase.save(record!, completionHandler:
                        ({returnRecord, error in
                            if let err = error {
                                print("Save Error: \(err.localizedDescription)")
                            } else {
//                                print("Success: Record \(returnRecord!.recordID.recordName) updated successfully")
                            }
                        })
                    )
                }else if(savedHighscore > self.previousHighScore){
//                    print("Level loading")
                    //Set game highscore equal to database highscore
                    self.defaults.set(savedHighscore, forKey: "PreviousHighScore")
                    self.previousHighScore = self.defaults.integer(forKey: "PreviousHighScore")
                    self.saveHighScore(self.leaderboardID, score: self.previousHighScore)
                    self.pauseMenuLevel = self.previousHighScore + 1
                    self.pauseScene()
                    self.resumeFromLevelSelect()
                    self.checkForLevelAchievements()
                }else{
                    //nothing
                }
            }
          }))
    }
    
    func share(){
        let message = "I just beat Bloop \(level - 1) in Blooper!"
        let image = currentScreenshot
        //let link = NSURL(fileURLWithPath: "https://developers.facebook.com")
        let shareContent:[AnyObject] = [message as AnyObject,image!]
        self.displayShareSheet(shareContent)
    }
    
    func screenshot(_ alertView:SCLAlertView) -> UIImage{
        UIGraphicsBeginImageContextWithOptions(self.view!.bounds.size, true, UIScreen.main.scale)
        self.view?.drawHierarchy(in: self.view!.bounds, afterScreenUpdates: true)
        alertView.view.drawHierarchy(in: alertView.view.bounds, afterScreenUpdates: true)
        let image:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext();
        
//        UIGraphicsBeginImageContext(CGSize(width: screenWidth * 0.8, height: screenWidth * 0.8))
//        image.drawAtPoint(CGPoint(x: screenWidth * -0.10, y: screenHeight * -0.27))
//        let croppedImage = UIGraphicsGetImageFromCurrentImageContext()
//        UIGraphicsEndImageContext();
        
        
        return image
    }
    
    func spawnPullForce(){
        let goalPullForceRadius = goalNumberBall.size.width * 5
        goalPullForce.region = SKRegion(radius: Float(goalPullForceRadius))
        goalPullForce.isEnabled = false
        goalPullForce.strength = pullStrength
        goalPullForce.falloff = 0
        goalPullForce.minimumRadius = Float(screenWidth * 2)
        goalPullForce.position = goalNumberBall.position
        goalPullForce.name = "goalPull"
        self.addChild(goalPullForce)
    }
    
    func spawnWalls(){
        let rectSize = CGSize(width: screenWidth + (wallDistFromEdge*2), height: screenHeight + (wallDistFromEdge*2))
        let rectPosition = CGPoint(x: (screenWidth / 2) - wallDistFromEdge, y: (screenHeight / 2) - wallDistFromEdge)
        let rect = SKShapeNode(rectOf: rectSize)
        rect.name = "wall"
        rect.lineWidth = 1
        rect.strokeColor = UIColor.black
        rect.position = rectPosition
        rect.physicsBody = SKPhysicsBody(edgeLoopFrom: CGRect(origin: CGPoint(x: -screenWidth / 2 , y: -screenHeight / 2), size: rectSize))
        rect.physicsBody?.affectedByGravity = false
        rect.physicsBody?.isDynamic = false
        rect.physicsBody?.categoryBitMask = PhysicsCategory.wall
        rect.physicsBody!.contactTestBitMask = PhysicsCategory.ball
        rect.physicsBody!.collisionBitMask = PhysicsCategory.ball
        rect.physicsBody!.friction = 0
        rect.physicsBody?.restitution = 0
        rect.physicsBody!.mass = 0
        self.addChild(rect)
    }
    
    func adjustBackgroundColor(){
        
        let backgroundNum:Int = level % (colorScheme.count * colorScheme[0].count)
        
        if(backgroundNum == 0){
            self.backgroundColor = colorScheme[colorScheme.count-1][colorScheme[0].count-1]
        }else{
        
            let schemeIndex:CGFloat = CGFloat(backgroundNum) / CGFloat(colorScheme[0].count)
            let schemeIndexInt:Int
            let shadeIndexInt:Int
            
            if(schemeIndex == floor(schemeIndex)){
                self.backgroundColor = colorScheme[Int(schemeIndex - 1)][colorScheme[0].count - 1]
            }else{
                schemeIndexInt = Int(floor(CGFloat(backgroundNum) / CGFloat(colorScheme[0].count)))
                shadeIndexInt = ((backgroundNum % colorScheme[0].count) - 1)
                self.backgroundColor = colorScheme[schemeIndexInt][shadeIndexInt]
            }
        }
        
//        self.backgroundColor = colorScheme[Int(arc4random_uniform(UInt32(colorScheme.count)))][
//            Int(arc4random_uniform(UInt32(colorScheme[0].count - 4)))+4]
        
        //self.backgroundColor = colorScheme[schemeIndex][shadeIndex]
        self.levelLabel.fontColor = ColorScheme.getComplementary(self.backgroundColor)
        
        self.pauseButton.strokeColor = UIColor.black
        self.pauseLabel.fontColor = UIColor.black
        
        let filterValue = Int(floor(CGFloat(level) / CGFloat(colorScheme.count * colorScheme[0].count)))
        if(filterValue > 1){
            applyBackgroundFilter(filterValue)
        }
        
    }
    
    func applyBackgroundFilter(_ filterValue:Int){
        
    }
    
    func adjustWall(){
        self.enumerateChildNodes(withName: "wall", using: { node,stop in
            node.removeFromParent()
        })
        wallDistFromEdge = (minimumRadius + CGFloat(level)) * 1.0 * screenAdjustment
        spawnWalls()
    }
    
    func adjustBallVelocity(){
        ballVelocityRange = [initialBallVelocityRange[0] + (CGFloat(level-1)*0.3), initialBallVelocityRange[1] - (CGFloat(level-1)*0.3)]
    }
    
    func adjustSpawnFrequency(){
        let potentialFrequency:CGFloat = initialSpawnFrequency - (log10(CGFloat(level)) / (2.8)) // change based on level
        
        if(potentialFrequency > minimumSpawnFrequency){
            spawnFrequency = potentialFrequency
        }else{
            spawnFrequency = minimumSpawnFrequency
        }
    }
    
    func spawnBalls(){
        for _ in 1...numBalls{
            balls.append(spawnRandomBall())
        }
    }
    
    func spawnRandomBall() -> SKSpriteNode{
        var isPowerUp:Bool = false
        var isPowerDown:Bool = false
        var randomPoint:CGPoint = CGPoint()
        var pointInGoalBall = true
        var counter = 0
        //var colour = ballColor
        
        let random = Int(arc4random_uniform(100))+1
        if((random <= powerUpPercentage) && (powerUpsEnabled) && level >= powerUpIncrement){
            isPowerUp = true
            //colour = powerUpColor
        }else if((random <= (powerUpPercentage + powerDownPercentage)) && (level >= powerUpIncrement)){
            isPowerDown = true
            //colour = powerDownColor
        }
        
        while(pointInGoalBall==true && counter < 100){
            var leftOrRightX = [-wallDistFromEdge, screenWidth]
            var leftOrRightY = [-wallDistFromEdge, screenHeight]
            let randomX = leftOrRightX[Int(arc4random_uniform(2))]
            let randomY = leftOrRightY[Int(arc4random_uniform(2))]
            randomPoint = CGPoint(
                x: CGFloat(arc4random_uniform(
                    UInt32(wallDistFromEdge))) + (randomX),
                y: CGFloat(arc4random_uniform(
                    UInt32(wallDistFromEdge))) + (randomY))
            
            pointInGoalBall = false
            for node in nodes(at: randomPoint){
                if(node.name == "goalNumberBall" || node.name == "startButtonBall"){
                    pointInGoalBall = true
                }
            }
            counter += 1
        }
        
        let ballNum = arc4random_uniform(UInt32(level))+1
        
        return spawnBall(radius: ((minimumRadius + (CGFloat(ballNum)/3)) * screenAdjustment),
                         xIn: randomPoint.x,
                         yIn: randomPoint.y,
                         name: "ball",
                         text: String(ballNum),
                         fontSize: 20,
                         fontColor: ColorScheme.colorWithHexString("737373"),
                         isMobile: true,
                         categoryBitMask: PhysicsCategory.ball,
                         isPowerUp: isPowerUp,
                         isPowerDown: isPowerDown)
    }
    
    func spawnBall(_ colorIn:UIColor = UIColor.clear, radius:CGFloat, xIn:CGFloat, yIn:CGFloat, name:String = "ball", text:String, fontName:String = "GeezaPro-Bold", fontSize:CGFloat = 20, fontColor:UIColor, zPos:CGFloat = 104, isMobile:Bool = true, categoryBitMask:UInt32 = PhysicsCategory.ball, isPowerUp:Bool = false, isPowerDown:Bool = false) -> SKSpriteNode{
        let tex:SKTexture
        if(isPowerUp){
            tex = SKTexture(imageNamed: "greenball1.png")
        }else if(isPowerDown){
            tex = SKTexture(imageNamed: "redball.png")
        }else if(isMobile == false){
            tex = SKTexture(imageNamed: "lightblueball.png")
        }else{
            tex = SKTexture(imageNamed: "orangeball.png")
        }
        
        let size = CGSize(width: radius*2, height: radius*2)
        // MINIMUM RADIUS
        
        let ball = SKSpriteNode(texture: tex, color: UIColor.clear, size: size)
        let ballLabel = SKLabelNode(text: text)
        
        if(isPowerUp){
            ball.name = "powerUp"
            ballLabel.text = ""
        }else{
            ball.name = name
        }
        
        if(isPowerDown){
            ball.name = "powerDown"
            ballLabel.text = ""
        }
        
        //ballLabel.fontName = "Arial Rounded MT Bold"
        ballLabel.fontName = fontName
        ballLabel.fontSize = fontSize
        ballLabel.position = CGPoint(x: 0, y: 0)
        ballLabel.fontColor = fontColor
        ballLabel.name = "ballLabel"
        ballLabel.horizontalAlignmentMode = .center
        ballLabel.verticalAlignmentMode = .center
        ball.addChild(ballLabel)
        ballLabel.zPosition = 1
        let positionIn = CGPoint(x: xIn, y: yIn)
        ball.zPosition = zPos
        ball.alpha = 0.9
        ball.physicsBody = SKPhysicsBody(circleOfRadius: radius)
        if(isMobile){
            ball.physicsBody!.isDynamic = true
            ball.physicsBody!.affectedByGravity = true
            ball.physicsBody!.allowsRotation = true
            ball.physicsBody?.isResting = false
            ball.physicsBody!.restitution = 1
            ball.physicsBody?.velocity = randomInverseScaleVector(Int(text)!)
            if(ballsSlowed){
                affectedBalls.append(ball)
                ball.physicsBody?.velocity.dx *= 0.25
                ball.physicsBody?.velocity.dy *= 0.25
            }
            ball.physicsBody?.mass = CGFloat(Int(text)!)
        }else{
            
            ball.physicsBody!.isDynamic = false
            ball.physicsBody!.affectedByGravity = false
            ball.physicsBody!.allowsRotation = false
            ball.physicsBody?.isResting = true
            ball.physicsBody!.restitution = 0
        }
        ball.physicsBody?.categoryBitMask = categoryBitMask
        ball.physicsBody!.contactTestBitMask = PhysicsCategory.wall | PhysicsCategory.ball | PhysicsCategory.goalNumberBall
        ball.physicsBody!.collisionBitMask = PhysicsCategory.wall | PhysicsCategory.ball | PhysicsCategory.goalNumberBall
        ball.physicsBody!.friction = 0
        ball.physicsBody?.linearDamping = 0
        ball.physicsBody?.angularDamping = 0
        ball.position = positionIn
        addChild(ball)
        
        if(name == "startButtonBall"){
            ballLabel.fontColor = UIColor.white
            ball.color = UIColor.gray
            ball.colorBlendFactor = 1.0
        }else{
            ballLabel.fontColor = UIColor.black
            ball.colorBlendFactor = 0.0
        }
        
        return ball
    }
    
    func touchBeganHandler(_ touchLocation:CGPoint) -> String{
        if(pauseButton.frame.contains(touchLocation)){
            if(pauseButton.isHidden == false){
                pauseButton.alpha = 1.0
                pressPause()
            }
        }else{
            isPlaying()
        }
        if(gameStarted){
            for node in nodes(at: touchLocation){
                
                if(["ball","powerUp","powerDown","goalNumberBall"].contains(node.name!)){
                    
                    var ballSpriteNode = node as! SKSpriteNode

                    if(node.name == "ball"){
                        if(ballsImmune == false){
                            removeBall(&ballSpriteNode, increment: false)
                            self.playSound(popSound)
                        }
                        return "ball"
                    }else if(node.name == "powerUp"){
                        if(ballsImmune == false){
                            activatePowerUp()
                            removeBall(&ballSpriteNode, increment: false)
                        }
                        return "powerUp"
                    }else if(node.name == "powerDown"){
                        activatePowerDown()
                        removeBall(&ballSpriteNode, increment: false)
                        return "powerDown"
                    }else if(node.name == "goalNumberBall"){
                        goalBallTouched()
                        //goalNumberBall.fillColor = colorWithHexString("ff3300")
                        goalNumberBall.run(changeImageToRedBall)
                        return "goalNumberBall"
                    }
                }
            }
        }else{
            for node in nodes(at: touchLocation){
                if(node.name == "startButtonBall"){
                    pauseButton.isHidden = true
                    gameStarted = true
                    goalPullForce.isEnabled = true
                    goalNumberBall.name = "goalNumberBall"
                    currentGoalNumber = randomGoalBallSize()
                    updateGoalBall()
                    //changeGoalBallText("\(currentGoalNumber)")
                    changeGoalBallFont("GeezaPro-Bold")
                    ballSpawnTimer = Timer.scheduledTimer(timeInterval: Double(spawnFrequency), target: self, selector: #selector(spawnRandomBall), userInfo: nil, repeats: true)
                    return "startButtonBall"
                }
            }
        }
        return ""
    }
    
    func touchMovedHandler(_ touchLocation:CGPoint) -> String{
        
        if(gameStarted){
            for node in nodes(at: touchLocation){
                
                
                if(["ball","powerUp","powerDown"].contains(node.name!)){

                    var ballSpriteNode = node as! SKSpriteNode

                    if(node.name == "ball" && swipingEnabled){
                        if(ballsImmune == false){
                            self.playSound(popSound)
                            removeBall(&ballSpriteNode, increment: false)}
                        return "ball"
                    }else if(node.name == "powerUp"){
                        if(ballsImmune == false){
                            activatePowerUp()
                            removeBall(&ballSpriteNode, increment: false)}
                        return "powerUp"
                    }else if(node.name == "powerDown"){
                        activatePowerDown()
                        removeBall(&ballSpriteNode, increment: false)
                        return "powerDown"
                    }
                }
            }
            moveTouchToDrawWall(0, wallPointsIndex: 0, wallLengthIndex: 0, maxWallLengthIndex: 0, lineWidth:5, withPhysics: false, touchLocation: touchLocation)
            isPlaying()
        }
        return ""
    }
    
    func moveTouchToDrawWall(_ wallIndex:Int, wallPointsIndex:Int, wallLengthIndex:Int, maxWallLengthIndex:Int, lineWidth:Int, withPhysics:Bool, touchLocation:CGPoint){
        //Draw wall
        walls[wallIndex].removeFromParent()
        walls[wallIndex] = SKShapeNode()
        walls[wallIndex].name = "userWall"
        wallPoints[wallPointsIndex].append(CGPoint(x: (touchLocation.x), y: (touchLocation.y)))
        while(wallLengths[wallLengthIndex] > maxWallLengths[maxWallLengthIndex]){
            wallLengths[wallLengthIndex] -= dist(
                wallPoints[wallPointsIndex][0],
                point2: wallPoints[wallPointsIndex][1])
            wallPoints[wallPointsIndex].removeFirst()
        }
        drawWall(wallIndex, wallPointsIndex: wallPointsIndex, wallLengthIndex: wallLengthIndex, lineWidth: lineWidth, withPhysics: withPhysics)
    }
    
    func removeTrail(){
        walls[0].removeFromParent()
        walls[0] = SKShapeNode()
        wallLengths[0] = 0.0
        wallPoints[0].removeAll()
    }
    
    func removeUserWall(){
        walls[1].removeFromParent()
        walls[1] = SKShapeNode()
        wallLengths[1] = 0.0
        wallPoints[1].removeAll()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchLocation = touches.first?.location(in: self)
        if(touchBeganHandler(touchLocation!) == "" && drawWallMode == true){
            //reset wall
            removeUserWall()
        }
        removeTrail()
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touchLocation = touches.first?.location(in: self)
        if(touchMovedHandler(touchLocation!) == "" && drawWallMode == true){
            moveTouchToDrawWall(1, wallPointsIndex: 1, wallLengthIndex: 1, maxWallLengthIndex: 1, lineWidth: 10, withPhysics: true, touchLocation: touchLocation!)
        }
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        pauseButton.alpha = 0.7
        
        if(drawWallMode == true){
            removeUserWall()
        }
        
        goalNumberBall.run(changeImageToLightBlueBall)
        if(doubleTap){
            goalBallTouched()
        }
        
        removeTrail()
        if(gameStarted){
            let touchLocation = touches.first?.location(in: self)
            for node in nodes(at: touchLocation!){
                
                if(["ball","powerUp","powerDown"].contains(node.name!)){
                    var ballSpriteNode = node as! SKSpriteNode
                    
                    if(node.name == "ball"){
                        if(ballsImmune == false){
                            removeBall(&ballSpriteNode, increment: false)
                        }
                    }else if(node.name == "powerUp"){
                        activatePowerUp()
                        removeBall(&ballSpriteNode, increment: false)
                    }else if(node.name == "powerDown"){
                        activatePowerDown()
                        removeBall(&ballSpriteNode, increment: false)
                    }
                }
            }
        }
    }
    
    func didBegin(_ contact: SKPhysicsContact) {
        var ball = contact.bodyA
        var other = contact.bodyB
        if(contact.bodyA.categoryBitMask > contact.bodyB.categoryBitMask){
            ball = contact.bodyB
            other = contact.bodyA
        }
        // Now ball is smaller body
        
        if(other.node?.name == "goalNumberBall"){
            var increment = true
            if(ball.node?.name == "powerUp" || ball.node?.name == "powerDown"){
                increment = false
            }
            self.playSound(biteSound)
            if(ball.node != nil){
                var ballSpriteNode = ball.node as! SKSpriteNode
                self.removeBall(&ballSpriteNode, increment: increment, animated: false)
            }
            self.run(SKAction.sequence([
                SKAction.run({
                    //self.goalNumberBall.fillColor = self.colorWithHexString("0066ff")
                    self.goalNumberBall.run(self.changeImageToDarkBlueBall)
                    
                }),
                SKAction.wait(forDuration: 0.1),
                SKAction.run({
                    let changeImageToLightBlueBall = SKAction.setTexture(SKTexture(imageNamed: "lightblueball.png"))
                    self.goalNumberBall.run(changeImageToLightBlueBall)
                })
                ]))
        }else if(other.node?.name == "userWall"){
            self.playSound(popSound)
            if(ball.node != nil){
                if(ball.node?.name == "powerUp"){
                    activatePowerUp()
                }else if(ball.node?.name == "powerDown"){
                    activatePowerDown()
                }
                //sometimes ball is nil
                var ballSpriteNode = ball.node as! SKSpriteNode
                self.removeBall(&ballSpriteNode, increment: false)
            }
        }
    }
    
    func randomVector() -> CGVector{
        return CGVector(
            dx: ballVelocityRange[Int(arc4random_uniform(2))],
            dy: ballVelocityRange[Int(arc4random_uniform(2))]
        )
    }
    
    func randomInverseScaleVector(_ scale:Int) -> CGVector{
        let adjustedScale = scale - 1
        var vector:CGVector = randomVector()
        vector.dx = vector.dx * CGFloat(CGFloat(level - adjustedScale) / CGFloat(level))
        vector.dy = vector.dy * CGFloat(CGFloat(level - adjustedScale) / CGFloat(level))
        
        if(vector.dx < 0 && vector.dx < -maximumVelocity){
            vector.dx = -maximumVelocity
        }else if(vector.dx > 0 && vector.dx > maximumVelocity){
            vector.dx = maximumVelocity
        }
        if(vector.dy < 0 && vector.dy < -maximumVelocity){
            vector.dy = -maximumVelocity
        }else if(vector.dy > 0 && vector.dy > maximumVelocity){
            vector.dy = maximumVelocity
        }
        
        if(vector.dx < 0 && vector.dx > -minimumVelocity){
            vector.dx = -minimumVelocity
        }else if(vector.dx > 0 && vector.dx < minimumVelocity){
            vector.dx = minimumVelocity
        }
        if(vector.dy < 0 && vector.dy > -minimumVelocity){
            vector.dy = -minimumVelocity
        }else if(vector.dy > 0 && vector.dy < minimumVelocity){
            vector.dy = minimumVelocity
        }
        //print("Ball: \(scale), Speed: \(vector)")
        return vector
    }
    
    func randomGoalBallSize() -> Int{
        return Int(arc4random_uniform(numberRange[1]-numberRange[0]) + numberRange[0])
    }
    
    func removeBall(_ ball:inout SKSpriteNode, increment:Bool, animated:Bool=true){
        if(increment){
            let ballNum:Int = Int((ball.childNode(withName: "ballLabel") as! SKLabelNode).text!)!
            currentGoalNumber += Int((Double(ballNum) * ballNumReduction))
            (goalNumberBall.childNode(withName: "ballLabel") as! SKLabelNode).text = String(currentGoalNumber)
            updateGoalBall()
        }
        ball.physicsBody = nil
        ball.name = "removedBall"
        //shrinkBallAnimation(&ball)
        if(animated){
            expandFadeAnimation(ball)
        }else{
            ball.removeFromParent()
        }
    }
    
    func expandFadeAnimation(_ ball:SKSpriteNode){
        let ballWidth = ball.frame.width
        ball.run(
            SKAction.repeat(
                SKAction.sequence([
                    SKAction.run({
                        if(ball.frame.width < ballWidth * 2){
                            ball.xScale += 0.01
                            ball.yScale += 0.01
                            ball.alpha -= 0.01
                            ball.childNode(withName: "ballLabel")?.xScale -= 0.01
                            ball.childNode(withName: "ballLabel")?.yScale -= 0.01
                        }else{
                            ball.removeFromParent()
                        }
                    }),
                    SKAction.wait(forDuration: 0.0007)
                    ]),count: 200
            )
        )    }
    
    func shrinkBallAnimation(_ ball:SKSpriteNode){
        ball.run(
            SKAction.repeat(
                SKAction.sequence([
                    SKAction.run({
                        if(ball.frame.width > 1){
                            ball.xScale -= 0.01
                            ball.yScale -= 0.01
                            ball.childNode(withName: "ballLabel")?.xScale -= 0.01
                            ball.childNode(withName: "ballLabel")?.yScale -= 0.01
                        }else{
                            ball.removeFromParent()
                        }
                    }),
                SKAction.wait(forDuration: 0.001)
                ]),count: 100
            )
        )
    }
    
    func updateGoalBall(){
        goalNumberBall.removeFromParent()
        if(currentGoalNumber <= 0 || (currentGoalNumber + Int(minimumRadius))*2 >= Int(screenHeight)){
            gameOver(false)
        }else{
            if(gameStarted){
                spawnGoalBall(true)
            }else{
                spawnGoalBall(false)
            }
        }
    }
    
    func spawnGoalBall(_ started:Bool){
        var fontSize:CGFloat
        let name:String
        let text:String
        var fontName:String
        let fontColor:UIColor = UIColor.black
        if(started){
            fontName = "GeezaPro-Bold"
            name = "goalNumberBall"
            text = "\(currentGoalNumber)"
            fontSize = 20.0
        }else{
            name = "startButtonBall"
            fontName = "Arial Rounded MT Bold"
            text = defaultGoalBallText
            if(currentGoalNumber < 10){
                fontSize = 15
            }else if(currentGoalNumber < 20){
                fontSize = CGFloat(10 + CGFloat(currentGoalNumber)*0.5)
            }else{
                fontSize = 20
            }
        }
        goalNumberBall = spawnBall(radius: CGFloat(
            UInt32(currentGoalNumber) + UInt32(minimumRadius)) * (screenAdjustment),
                                   xIn: self.frame.midX,
                                   yIn: self.frame.midY,
                                   name: name,
                                   text: text,
                                   fontName: fontName,
                                   fontSize: fontSize,
                                   fontColor: fontColor,
                                   isMobile: false,
                                   categoryBitMask: PhysicsCategory.goalNumberBall,
                                   isPowerUp: false,
                                   isPowerDown: false)
    }
    
    func gameOver(_ restart:Bool){
        ballSpawnTimer.invalidate()
        powerUpTimer.invalidate()
        powerDownTimer.invalidate()
        enumerateChildNodes(withName: "powerUpLabelShadow", using: { node,stop in
            (node as! SKLabelNode).text = ""
        })
        enumerateChildNodes(withName: "powerDownLabelShadow", using: { node,stop in
            (node as! SKLabelNode).text = ""
        })
        powerUpLabel.text = ""
        powerDownLabel.text = ""
        goalPullForce.isEnabled = false
        gameStarted = false
        
        if(currentGoalNumber <= 0 && restart == false){
            gameWon()
        }else{
            gameLost(restart)
        }
    }
    
    func removeAllPowerUpsAndDowns(){
        powerUpTimer.invalidate()
        powerDownTimer.invalidate()
        enumerateChildNodes(withName: "powerUpLabelShadow", using: { node,stop in
            (node as! SKLabelNode).text = ""
        })
        enumerateChildNodes(withName: "powerDownLabelShadow", using: { node,stop in
            (node as! SKLabelNode).text = ""
        })
        indicatorTimer.invalidate()
        adjustBackgroundColor()
        powerUpLabel.text = ""
        powerDownLabel.text = ""
        self.removeAllActions()
        walls[1].removeFromParent()
        ballsImmune = false
        doubleTap = false
        ballsFrozen = false
        ballsSlowed = false
        powerDown = false
        drawWallMode = false
        ballNumReduction = 1.0
    }
    
    func restartLevel(){
//        adjustBackgroundColor()
        levelsLost += 1
        restart(UIAlertAction())
    }
    
    func restart(_ alert:UIAlertAction){
        if(unlockedPowerup){
            powerUpAlert(powerUps.last!)
            unlockedPowerup = false
        }
        
        if(self.isPaused){
            self.isPaused = false
            self.gameStarted = false
        }
        
        removeUserWall()
        removeTrail()
        //levelLabel.text = "Bloop \(level)"
        levelLabel.text = "\(level)"
        enumerateChildNodes(withName: "levelLabelShadow", using: { node,stop in
            (node as! SKLabelNode).text = self.levelLabel.text
        })
        for node in self.children{
            if(["ball","powerUp","powerDown"].contains(node.name!)){
                node.removeFromParent()
            }
        }
        currentGoalNumber = randomGoalBallSize()
        updateGoalBall()
        goalNumberBall.name = "startButtonBall"
        changeGoalBallFont("Arial Rounded MT Bold")
        changeGoalBallText(defaultGoalBallText)
        spawnBalls()
        removeAllPowerUpsAndDowns() // ???
    }
    
    func changeGoalBallFont(_ fontName:String){
        goalNumberBall.enumerateChildNodes(withName: "ballLabel", using: { node,stop in
            (node as! SKLabelNode).fontName = fontName
        })
    }
    
    func earnAchievement(_ identifier:String, percentage:Double,showBannerIfPercentUnder100:Bool = false){
        let achievement = GKAchievement(identifier: identifier)
        achievement.percentComplete = percentage
        if(showBannerIfPercentUnder100 || percentage == 100){
//            print("Show completion banner")
            achievement.showsCompletionBanner = true
        }
        GKAchievement.report([achievement], withCompletionHandler: nil)
    }
    
    func checkForLevelAchievements(){
        if(level > previousHighScore){
            for levelNum in [1,2,10,20,50,100,200,300,400,500,1000]{
                if(levelNum >= level){
                    earnAchievement("level\(levelNum)", percentage: ((Double(level) * 100.0)/Double(levelNum)))
                }
            }
        }
    }
    
    func gameWon(){
        
        checkForLevelAchievements()
     
        self.playSound(popSound)
        self.playSound(winSound)
        if(level > previousHighScore && testingModeEnabled == false){
            previousHighScore = level
            defaults.set(level, forKey: "PreviousHighScore")
            saveHighScore(leaderboardID, score: previousHighScore)
            updateDatabase()
        }
        if(level == 1 && levelJump > 1){
            level += (levelJump-1)
        }else{
            level += levelJump
        }
        
        if(level % powerUpIncrement == 0){
            let newPowerUp = powerUpForLevel(level)
            if(newPowerUp != nil){
                powerUps.append(newPowerUp!)
                unlockedPowerup = true
            }
        }
        pauseMenuLevel = level
        levelAdjustments()
        launchWinAlertView()
        
        self.run(SKAction.sequence([
            SKAction.wait(forDuration: 0.2),
            SKAction.run({
                self.currentScreenshot = self.screenshot(self.winLevelPopup)
            })
        ]))
        
        levelsWon += 1
        
        if((levelsWon + levelsLost) >= 5){
            levelsWon = 0
            levelsLost = 0
            STAStartAppAdBasic.showAd()
        }

    }
    
//    func loadAchievements(){
//        GKAchievement.loadAchievementsWithCompletionHandler() { achievements, error in
//            guard let achievements = achievements else { return }
//        }
//    }
    
    func launchWinAlertView(){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        
        winLevelPopup = SCLAlertView(appearance: appearance)
        winLevelPopup.addButton("Next Bloop", target: self, selector: #selector(self.restart(_:)))
        winLevelPopup.addButton("Share", target: self, selector: #selector(self.share))
        winLevelPopup.showSuccess("Bloop \(level - 1) Completed", subTitle: "The Blob Was Destroyed!")
    }
    
    //
    func powerUpAlert(_ newPowerUp:PowerUp){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let alert:SCLAlertView = SCLAlertView(appearance: appearance)
        alert.addButton("Ok", target: self, selector: #selector(nothing))
        alert.showInfo("Unlocked Powerup!", subTitle: "\(newPowerUp.name): \(newPowerUp.unlockDescription)")
    }
    
    func gameLost(_ restart:Bool){
        goalNumberBall.removeFromParent()
        if(restart == false){
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alert:SCLAlertView = SCLAlertView(appearance: appearance)
            alert.addButton("Try Again", target: self, selector: #selector(self.restart(_:)))
            //alert.showError("Game Over", subTitle: "The Blob Got You!")
            alert.showError("Game Over", subTitle: "The Blob Got You!", duration: 1.0)
            self.restart(UIAlertAction())
            self.playSound(loseSound)
        }else{
            self.restart(UIAlertAction())
        }
        levelsLost += 1
        
        if((levelsLost + levelsWon) >= 5){
            levelsLost = 0
            levelsWon = 0
            STAStartAppAdBasic.showAd()
        }
        
    }
    
    func changeGoalBallText(_ text:String){
        (goalNumberBall.childNode(withName: "ballLabel") as! SKLabelNode).text = text
    }
    
    func textFromNode(_ node:SKNode) -> String{
        return (((node as! SKSpriteNode).childNode(withName: "ballLabel") as! SKLabelNode).text!)
    }
    
    func goalBallTouched(){
        if(currentGoalNumber != 0){
            if(powerDown == false){
                self.playSound(tickSound)
                currentGoalNumber -= shrinkPerClick
                if(currentGoalNumber <= 0){
                    self.playSound(popSound)
                }
                updateGoalBall()
            }else{
                self.playSound(powerDownTickSound)
            }
        }
    }
    
    func authenticateLocalPlayer(){
        let localPlayer = GKLocalPlayer()
        localPlayer.authenticateHandler = { (viewController, error) -> Void in
            if(viewController != nil){
                let uiViewController:UIViewController = (self.view?.window?.rootViewController)!
                uiViewController.present(viewController!, animated: true, completion: nil)
            }else{
                //print("Authentication is \(GKLocalPlayer.localPlayer().authenticated)")
            }
        }
    }
    
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: nil)
    }
    
    func showLeaderboard() {
        let gcViewController: GKGameCenterViewController = GKGameCenterViewController()
        gcViewController.gameCenterDelegate = self
        gcViewController.viewState = GKGameCenterViewControllerState.leaderboards
        gcViewController.leaderboardIdentifier = leaderboardID
        let uiViewController:UIViewController = (self.view?.window?.rootViewController)!
        uiViewController.present(gcViewController, animated: true, completion: nil)
    }
    
    func showGameCenter(){
        let gameCenterViewController = GKGameCenterViewController()
        gameCenterViewController.gameCenterDelegate = self
        let uiViewController:UIViewController = (self.view?.window?.rootViewController)!
        uiViewController.present(gameCenterViewController, animated: true, completion: nil)
    }
    
    func presentViewController(_ vc:UIViewController){
        let uiViewController:UIViewController = (self.view?.window?.rootViewController)!
        uiViewController.present(vc, animated: true, completion: nil)
    }
    
    func saveHighScore(_ identifier:String, score:Int){
        if(testingModeEnabled == false){
            if(GKLocalPlayer.localPlayer().isAuthenticated){
                let scoreReporter = GKScore(leaderboardIdentifier: identifier)
                scoreReporter.value = Int64(score)
                let scoreArray:[GKScore] = [scoreReporter]
                GKScore.report(scoreArray, withCompletionHandler: {
                    error -> Void in
                    if (error != nil){
                        print("error")
                    }else{
                        //handle new high score message
                    }
                })
            }
        }else{
//            print("Testing Mode: GameCenter not updated.")
        }
    }
    
    func powerUpForLevel(_ level:Int) -> PowerUp?{
        switch(level){
        case powerUpIncrement:
            //Balls travel half speed
            return PowerUp(nameIn: "Slo-Mo", descriptionIn: "Orange balls move at half speed.", timeIn: 3, soundActionIn: slowSound)
        case powerUpIncrement*2:
            // Draw a Wall
            return PowerUp(nameIn: "Waller", descriptionIn: "Your swipes leave a trail that pops any contacted orange balls.", timeIn: 5, soundActionIn: wallerSound)
        case powerUpIncrement*3:
            // Removes all balls from screen
            return PowerUp(nameIn: "Clear", descriptionIn: "All orange balls on the screen are popped.", timeIn: 3, soundActionIn: clearSound)
        case powerUpIncrement*4:
            return PowerUp(nameIn: "Freeze", descriptionIn: "All orange balls are frozen and new ones stop spawning.", timeIn: 2, soundActionIn: freezerSound)
        // Doubles tapping power
        case powerUpIncrement*5:
            return PowerUp(nameIn: "Armor", descriptionIn: "The blue ball's growth from contacted orange balls is reduced.", timeIn: 5, soundActionIn: armorSound)
        // Growth reduced by half
        case powerUpIncrement*6:
            return PowerUp(nameIn: "DoubleTap", descriptionIn: "Each tap on the blue ball does double damage.", timeIn: 3, soundActionIn: doubleTapSound)
        // Doubles tapping power
        default:
            return nil
        }
    }
    
    func loadPowerUps(_ allPowerUps:Bool){
        var count = powerUpIncrement
        powerUps.removeAll()
        if(allPowerUps == false){
            while(count <= level && count <= powerUpMaxLevel){
                powerUps.append(powerUpForLevel(count)!)
                count += powerUpIncrement
            }
        }else{
            while(count <= powerUpMaxLevel){
                powerUps.append(powerUpForLevel(count)!)
                count += powerUpIncrement
            }
        }
    }
    
    func indicator(){
        self.backgroundColor = UIColor.black
        indicatorTimer.invalidate()
        indicatorTimer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(self.adjustBackgroundColor), userInfo: nil, repeats: false)
    }
    
    func activatePowerDown(){
        indicator()
        self.playSound(powerDownSound)
        powerDown = true
        self.removeAction(forKey: "powerDown")
        self.run(SKAction.sequence([
            SKAction.wait(forDuration: 5.0),
            SKAction.run({
                self.powerDown = false
                self.pauseButton.strokeColor = UIColor.black
                self.pauseLabel.fontColor = UIColor.black
            })
        ]), withKey: "powerDown")
        powerDownTimeLeft = 5
        powerDownLabel.text = "PowerDown: \(powerDownTimeLeft)"
        enumerateChildNodes(withName: "powerDownLabelShadow", using: { node,stop in
            (node as! SKLabelNode).text = "PowerDown: \(self.powerDownTimeLeft)"
        })
        
        self.powerUpTimer.invalidate()
        self.powerUpLabel.text = ""
        enumerateChildNodes(withName: "powerUpLabelShadow", using: { node,stop in
            (node as! SKLabelNode).text = ""
        })
        self.powerDownTimer.invalidate()
        self.powerDownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatePowerDownLabel), userInfo: nil, repeats: true)
        
        self.pauseButton.strokeColor = UIColor.white
        self.pauseLabel.fontColor = UIColor.white
    }
    
    func activatePowerUp(){
        if(arc4random_uniform(100) <= 20){
            currentPowerUp = powerUps.last!
        }else{
            currentPowerUp = powerUps[Int(arc4random_uniform(UInt32(powerUps.count)))]
        }
        self.playSound(currentPowerUp.soundAction)
        let powerUpEffects = currentPowerUp.getPowerUpInfo()
        powerUpTimeLeft = currentPowerUp.time
        
        for effect in powerUpEffects{
            switch(effect.0){
            case "shrinkPerClick":
                //shrinkPerClick = effect.1
                doubleTap = true
                self.run(SKAction.sequence([
                    SKAction.wait(forDuration: Double(currentPowerUp.time)),
                    SKAction.run({
                        self.doubleTap = false
                        //self.shrinkPerClick = self.originalShrinkPerClick
                    })
                ]))
                
            case "stopMovement":
                if(ballsFrozen == false){
                    lastVelVectors = [CGVector]()
                    ballsFrozen = true
                }else{
                    self.removeAction(forKey: "stopMovement")
                }
                self.run(SKAction.sequence([
                    SKAction.run({
                        for node in self.children{
                            if(["ball","powerUp","powerDown"].contains(node.name!)){
                                self.lastVelVectors.append(node.physicsBody!.velocity)
                                node.physicsBody!.isDynamic = false
                            }
                        }
                        //self.ballsImmune = true TO BE DECIDED
                    }),
                    SKAction.wait(forDuration: Double(currentPowerUp.time)),
                    SKAction.run({
                        self.ballsFrozen = false
                        for node in self.children{
                            if(["ball","powerUp","powerDown"].contains(node.name!)){
                                if(self.gameStarted){
                                    node.physicsBody!.isDynamic = true
                                    if(self.lastVelVectors.first != nil){
                                        node.physicsBody?.velocity = self.lastVelVectors.first!
                                        self.lastVelVectors.removeFirst()
                                    }
                                }else{
                                    if(node.physicsBody!.isDynamic == false){
                                        node.removeFromParent()
                                    }
                                }
                            }
                        }
                        self.ballsImmune = false
                    })
                ]), withKey: "stopMovement")
                
            case "stopSpawning":
                self.removeAction(forKey: "stopSpawning")
                self.ballSpawnTimer.invalidate()
                self.run(SKAction.sequence([
                    SKAction.wait(forDuration: Double(currentPowerUp.time)),
                    SKAction.run({
                        if(self.ballSpawnTimer.isValid == false && self.gameStarted){
                            self.ballSpawnTimer = Timer.scheduledTimer(timeInterval: Double(self.spawnFrequency), target: self, selector: #selector(self.spawnRandomBall), userInfo: nil, repeats: true)
                        }
                    })
                ]), withKey: "stopSpawning")
                
            case "clear":
                for node in self.children{
                    if(["ball","powerUp","powerDown"].contains(node.name!)){
                        //node as! SKSpriteNode
                        var ballSpriteNode = node as! SKSpriteNode
                        removeBall(&ballSpriteNode, increment: false)
                    }
                }
                
            case "armor":
                ballNumReduction  = (20.0 / Double(level))
                self.removeAction(forKey: "armor")
                self.run(SKAction.sequence([
                    SKAction.wait(forDuration: Double(currentPowerUp.time)),
                    SKAction.run({
                        self.ballNumReduction = 1.0
                    })
                    ]), withKey: "armor")
                
            case "wall":
                if(drawWallMode == false){
                    removeUserWall()
                }
                drawWallMode = true
                removeTrail()
                self.removeAction(forKey: "wall")
                self.run(SKAction.sequence([
                    SKAction.wait(forDuration: Double(currentPowerUp.time)),
                    SKAction.run({
                        self.drawWallMode = false
                        self.enumerateChildNodes(withName: "userWall", using: { node,stop in
                            node.removeFromParent()
                        })
                    })
                ]), withKey: "wall")
                
            case "slow":
                if(ballsFrozen){
                    self.removeAction(forKey: "stopMovement")
                    ballsFrozen = false
                }
                if(ballsSlowed == false){
                    ballsSlowed = true
                }else{
                    self.removeAction(forKey: "slow")
                    for node in self.affectedBalls{
                        if(self.gameStarted){
                            if(node != nil){
                                node!.physicsBody?.velocity.dx *= 3.0
                                node!.physicsBody?.velocity.dy *= 3.0
                            }
                        }
                    }
                }
                affectedBalls.removeAll()
                
                self.run(SKAction.sequence([
                    SKAction.run({
                        for node in self.children{
                            if(["ball","powerUp","powerDown"].contains(node.name!)){
                                self.affectedBalls.append(node)
                                node.physicsBody?.velocity.dx *= 0.25
                                node.physicsBody?.velocity.dy *= 0.25
                            }
                        }
                    }),
                    SKAction.wait(forDuration: Double(currentPowerUp.time)),
                    SKAction.run({
                        self.ballsSlowed = false
                        for node in self.affectedBalls{
                            if(self.gameStarted){
                                if(node != nil){
                                    node!.physicsBody?.velocity.dx *= 3.0
                                    node!.physicsBody?.velocity.dy *= 3.0
                                }
                            }
                        }
                    })
                ]), withKey: "slow")
                
            default:
                break
            }
            
            self.powerUpTimer.invalidate()
            self.powerDownTimer.invalidate()
            self.powerDownLabel.text = ""
            enumerateChildNodes(withName: "powerDownLabelShadow", using: { node,stop in
                (node as! SKLabelNode).text = ""
            })
            self.powerUpTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatePowerUpLabel), userInfo: nil, repeats: true)
            
        }
        if(["Clear"].contains(currentPowerUp.name) == false){
            enumerateChildNodes(withName: "powerUpLabelShadow", using: { node,stop in
                (node as! SKLabelNode).text = "\(self.currentPowerUp.name): \(self.powerUpTimeLeft)"
            })
            powerUpLabel.text = "\(currentPowerUp.name): \(powerUpTimeLeft)"
        }else{
            enumerateChildNodes(withName: "powerUpLabelShadow", using: { node,stop in
                (node as! SKLabelNode).text = "Clear"
            })
            powerUpLabel.text = "Clear"
        }
    }
    
    func updatePowerUpLabel(){
        powerUpTimeLeft -= 1
        if(powerUpTimeLeft > 0){
            if(["Clear"].contains(currentPowerUp.name) == false){
                enumerateChildNodes(withName: "powerUpLabelShadow", using: { node,stop in
                    (node as! SKLabelNode).text = "\(self.currentPowerUp.name): \(self.powerUpTimeLeft)"
                })
                powerUpLabel.text = "\(currentPowerUp.name): \(powerUpTimeLeft)"
            }else{
                enumerateChildNodes(withName: "powerUpLabelShadow", using: { node,stop in
                    (node as! SKLabelNode).text = "Clear"
                })
                powerUpLabel.text = "Clear"
            }
        }else{
            enumerateChildNodes(withName: "powerUpLabelShadow", using: { node,stop in
                (node as! SKLabelNode).text = ""
            })
            powerUpLabel.text = ""
        }
    }
    
    func updatePowerDownLabel(){
        powerDownTimeLeft -= 1
        if(powerDownTimeLeft > 0){
            enumerateChildNodes(withName: "powerDownLabelShadow", using: { node,stop in
                (node as! SKLabelNode).text = "PowerDown: \(self.powerDownTimeLeft)"
            })
            powerDownLabel.text = "PowerDown: \(powerDownTimeLeft)"
        }else{
            enumerateChildNodes(withName: "powerDownLabelShadow", using: { node,stop in
                (node as! SKLabelNode).text = ""
            })
            powerDownLabel.text = ""
        }
    }
    
    func makePathFromPointArray(_ pointArray:[CGPoint]) -> CGPath{
        let path = CGMutablePath()
        //CGPathMoveToPoint(path, [], (pointArray.first?.x)!, (pointArray.first?.y)!)
        path.move(to: CGPoint(x: (pointArray.first?.x)!, y: (pointArray.first?.y)!))
        if(pointArray.count>1){
            for point in pointArray{
                path.addLine(to: CGPoint(x: point.x, y: point.y))
            }
        }
        return path
    }
    
    func makePointArrayFromTupleArray(_ tuples:[(CGFloat,CGFloat)]) -> [CGPoint]{
        var pointArray = [CGPoint]()
        for tuple in tuples{
            pointArray.append(CGPoint(x: tuple.0, y: tuple.1))
        }
        return pointArray
    }
    
    func drawWall(_ wallIndex:Int, wallPointsIndex:Int, wallLengthIndex:Int, lineWidth:Int, withPhysics:Bool){
        let path = CGMutablePath()
        path.addLine(to: CGPoint(x: (wallPoints[wallPointsIndex].first?.x)!, y: (wallPoints[wallPointsIndex].first?.y)!))

        if(wallPoints[wallPointsIndex].count>1){
            let point1 = wallPoints[wallPointsIndex][wallPoints[wallPointsIndex].count-2]
            let point2 = wallPoints[wallPointsIndex][wallPoints[wallPointsIndex].count-1]
            wallLengths[wallLengthIndex] += dist(point1, point2: point2)
            for point in wallPoints[wallPointsIndex]{
                path.addLine(to: CGPoint(x: point.x, y: point.y))
            }
        }
        walls[wallIndex].path = path
        walls[wallIndex].lineCap = .round
        walls[wallIndex].lineJoin = .bevel
        walls[wallIndex].zPosition = 100
        walls[wallIndex].position = position
        walls[wallIndex].strokeColor = UIColor.black
        walls[wallIndex].lineWidth = CGFloat(lineWidth)
        if(withPhysics){
            walls[wallIndex].physicsBody = SKPhysicsBody(edgeChainFrom: path)
            walls[wallIndex].physicsBody?.restitution = 0.8
            walls[wallIndex].physicsBody?.affectedByGravity = false
            walls[wallIndex].physicsBody?.isDynamic = false
            walls[wallIndex].physicsBody?.categoryBitMask = PhysicsCategory.wall
            walls[wallIndex].physicsBody?.contactTestBitMask = PhysicsCategory.ball
            walls[wallIndex].physicsBody?.collisionBitMask = PhysicsCategory.ball
            walls[wallIndex].physicsBody?.mass = 10
        }
        
        self.addChild(walls[wallIndex])
        
        if(wallIndex == 0){
            
            if(ColorScheme.equal(backgroundColor, colour2: UIColor.white)){
                self.walls[wallIndex].strokeColor = neonGreen
            }else{
                self.walls[wallIndex].strokeColor = ColorScheme.getComplementary(self.backgroundColor)
            }
        }
    }
    
    func dist(_ point1:CGPoint, point2:CGPoint) -> CGFloat{
        let first = pow((point1.x - point2.x),2.0)
        let second = pow((point1.y - point2.y),2.0)
        return sqrt(first + second)
    }
    
    func pressPause(){
        pauseScene()
        pauseAlert = SCLAlertView(appearance: appearance)
        
        if(!musicIsPaused){
            pauseAlert.addButton("Music Off 🔇", target: self, selector: #selector(pauseMusic))
        }else{
            pauseAlert.addButton("Music On 🔊", target: self, selector: #selector(resumeMusic))
        }
        pauseAlert.addButton("Select Level", target: self, selector: #selector(levelSelect))
        pauseAlert.addButton("Main Menu", target: self, selector: #selector(mainMenu))
        if(currentScreenshot != nil){
            pauseAlert.addButton("Share", target: self, selector: #selector(share))
        }
        pauseAlert.addButton("Restart", target: self, selector: #selector(restartLevel))
        pauseAlert.addButton("Resume", target: self, selector: #selector(resumeScene))
        pauseAlert.showInfo("Pause Menu", subTitle: "Bloop: \(pauseMenuLevel)", circleIconImage: pauseIcon)
    }
    
    func mainMenu(){
        audioPlayer.stop()
        let menuScene = GameMenuScene(size: self.view!.frame.size)
        let skView = self.view!
        menuScene.scaleMode = .aspectFill
        skView.showsFPS = false
        skView.showsNodeCount = false
        skView.presentScene(menuScene)
    }
    
    func levelSelect(_ wrongInputIn:Bool){
        levelSelectAlert = SCLAlertView(appearance: appearance)
        levelSelectText = levelSelectAlert.addTextField("\(level)")
        levelSelectText.keyboardType = UIKeyboardType.numberPad // doesnt work for certain keyboards
        levelSelectAlert.addButton("Cancel", target: self, selector: #selector(pressPause))
        levelSelectAlert.addButton("Start", target: self, selector: #selector(loadLevel))
        if(wrongInputIn){
            levelSelectAlert.showEdit("Select Bloop", subTitle: "Bloop Must Be 1-\(previousHighScore+1)")
        }else{
            levelSelectAlert.showEdit("Select Bloop", subTitle: "")
        }
    }
    
    func loadLevel(){
        levelsLost += 1
        if(levelSelectText.text == ""){
            wrongInput = true
            levelSelect(wrongInput)
        }else if(Int(levelSelectText.text!)! > previousHighScore+1 || Int(levelSelectText.text!)! == 0){
            wrongInput = true
            levelSelect(wrongInput)
        }else{
            pauseMenuLevel = Int(levelSelectText.text!)!
            wrongInput = false
            resumeFromLevelSelect()
        }
    }
    
    func incrementPauseMenuText(){
        if(pauseMenuLevel <= defaults.integer(forKey: "PreviousHighScore")){
            pauseMenuLevel += 1
        }
        autoResumeScene()
        pressPause()
    }
    
    func decrementPauseMenuText(){
        if(pauseMenuLevel > 1){
            pauseMenuLevel -= 1
        }
        autoResumeScene()
        pressPause()
    }
    
    func playSongAVPlayer(_ soundName: String)
    {
        let sound = URL(fileURLWithPath: Bundle.main.path(forResource: soundName, ofType: "mp3")!)
        do{
            audioPlayer = try AVAudioPlayer(contentsOf:sound)
            audioPlayer.numberOfLoops = 999
            audioPlayer.prepareToPlay()
            run(SKAction.sequence([
                SKAction.wait(forDuration: 1.5),
                SKAction.run({
                    self.audioPlayer.play()
                    if(self.musicStartsPaused){
                        self.audioPlayer.pause()
                    }
                })
            ]))
        }catch {
            print("Error getting the audio file")
        }
    }
    
    func pauseScene(){
        ballSpawnTimer.invalidate()
        powerUpTimer.invalidate()
        powerDownTimer.invalidate()
        self.isPaused = true
    }
    
    func autoResumeScene(){
        self.isPaused = false
    }
    
    func resumeFromLevelSelect(){
        if(gameStarted){
            ballSpawnTimer = Timer.scheduledTimer(timeInterval: Double(spawnFrequency), target: self, selector: #selector(spawnRandomBall), userInfo: nil, repeats: true)
        }
        if(pauseMenuLevel != level){
            level = pauseMenuLevel
            levelAdjustments()
            powerUps.removeAll()
            loadPowerUps(false)
            gameOver(true)
        }
        self.isPaused = false
    }
    
    func resumeScene(){
        if(gameStarted){
            ballSpawnTimer = Timer.scheduledTimer(timeInterval: Double(spawnFrequency), target: self, selector: #selector(spawnRandomBall), userInfo: nil, repeats: true)
            powerUpTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatePowerUpLabel), userInfo: nil, repeats: true)
            powerDownTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(updatePowerDownLabel), userInfo: nil, repeats: true)
        }
        self.isPaused = false
    }
    
    func isPlaying(){
        pauseButton.isHidden = true
        self.childNode(withName: "pauseButtonShadow")?.isHidden = true
        hasClickedTimer.invalidate()
        hasClickedTimer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(notPlaying), userInfo: nil, repeats: false)
    }
    
    func notPlaying(){
        pauseButton.isHidden = false
        self.childNode(withName: "pauseButtonShadow")?.isHidden = false
    }
    
    func levelAdjustments(){
        adjustWall()
        adjustSpawnFrequency()
        adjustBallVelocity()
        adjustBackgroundColor()
        //adjustLevelLabel()
    }
    
    func adjustLevelLabel(){
        if(level > 99){
            levelLabel.fontSize = screenHeight * 0.3
        }else if(level > 9){
            levelLabel.fontSize = screenHeight * 0.5
        }else{
            levelLabel.fontSize = screenHeight
        }
    }
    
    func pauseMusic(){
        defaults.set(false, forKey: "musicOn")
        audioPlayer.pause()
        musicIsPaused = true
        autoResumeScene()
        pressPause()
    }
    
    func resumeMusic(){
        defaults.set(true, forKey: "musicOn")
        audioPlayer.play()
        musicIsPaused = false
        autoResumeScene()
        pressPause()
    }
    
    func setupLevel(){
        levelAdjustments()
        spawnGoalBall(false)
        spawnBalls()
        spawnPullForce()
        loadPowerUps(false)
    }
    
    func playSound(_ sound:SKAction, completion: @escaping ()->() = {}){
        if(disableAllSounds == false){
            self.run(sound, completion: completion)
        }else{
            SKAction.run(completion)
        }
    }
    
    func nothing(){
        
    }
    
    func moveLabelWithGradient(_ label:SKLabelNode, x:CGFloat, y:CGFloat){
        label.position = CGPoint(x: x, y: y)
        enumerateChildNodes(withName: "\(label.name!)Shadow", using: { node,stop in
            (node as! SKLabelNode).position = CGPoint(x: x + self.xOffset, y: y - self.yOffset)
        })
    }
    
}
