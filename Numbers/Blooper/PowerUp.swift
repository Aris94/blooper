//
//  PowerUp.swift
//  Blooper
//
//  Created by Aris Agdere on 5/8/16.
//  Copyright © 2016 aagdere1. All rights reserved.
//

import Foundation
import SpriteKit

class PowerUp{
    var name:String
    var unlockDescription:String
    var time:Int
    var soundAction:SKAction
    
    init(nameIn:String = "", descriptionIn:String = "", timeIn:Int = 1, soundActionIn:SKAction = SKAction()){
        name = nameIn
        unlockDescription = descriptionIn
        time = timeIn
        soundAction = soundActionIn
    }
    
    func getPowerUpInfo() -> [String:Int]{
        var changes = [String:Int]()
        
        if(name == "DoubleTap"){
            changes.updateValue(2, forKey: "shrinkPerClick")
        }else if(name == "Freeze"){
            changes.updateValue(0, forKey: "stopMovement")
            changes.updateValue(0, forKey: "stopSpawning")
            //changes.updateValue(0, forKey: "stopSpawning")
        }else if(name == "Clear"){
            changes.updateValue(0, forKey: "clear")
        }else if(name == "Armor"){
            changes.updateValue(2, forKey: "armor")
        }else if(name == "Waller"){
            changes.updateValue(1, forKey: "wall")
        }else if(name == "Slo-Mo"){
            changes.updateValue(1, forKey: "slow")
        }
        return changes
    }
    
}