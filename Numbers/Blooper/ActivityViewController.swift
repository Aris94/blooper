//
//  ActivityViewController.swift
//  Blooper
//
//  Created by Aris Agdere on 6/1/16.
//  Copyright © 2016 aagdere1. All rights reserved.
//

import Foundation
import UIKit

class ActivityViewController: UIActivityViewController {
    
    func _shouldExcludeActivityType(_ activity: UIActivity) -> Bool {
        let activityTypesToExclude = [
            "com.apple.reminders.RemindersEditorExtension",
            "com.apple.mobilenotes.SharingExtension",
            UIActivityType.openInIBooks,
            UIActivityType.print,
            UIActivityType.assignToContact,
            "com.google.Drive.ShareExtension"
        ] as [Any]
        
        let actType = activity.activityType;
        
        for type in activityTypesToExclude{
            if (actType == (type as! UIActivityType)){
                return true
            }
        }
        
        for type in super.excludedActivityTypes!{
            if (actType == type){
                return true
            }
        }
        return false
    }
    
}
